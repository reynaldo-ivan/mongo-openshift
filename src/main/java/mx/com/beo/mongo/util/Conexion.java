package mx.com.beo.mongo.util;

import java.util.Arrays;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

/**
 * Copyright (c) 2017 Nova Solution Systems S.A. de C.V. Mexico D.F. Todos los
 * derechos reservados.
 *
 * @author Reynaldo Ivan Martinez Lopez
 *
 *         ESTE SOFTWARE ES INFORMACIÓN CONFIDENCIAL. PROPIEDAD DE NOVA SOLUTION
 *         SYSTEMS. ESTA INFORMACIÓN NO DEBE SER DIVULGADA Y PUEDE SOLAMENTE SER
 *         UTILIZADA DE ACUERDO CON LOS TÉRMINOS DETERMINADOS POR LA EMPRESA SÍ
 *         MISMA.
 */


public class Conexion {
	
	public MongoClient crearConexion() {
		MongoClient mongo = null;
		
		String baseDatos=Urls.BASEDATOS_MONGO.getPath();
		String usu=Urls.USER_MONGO.getPath();
		String pwd=Urls.PWD_MONGO.getPath();
		
		String host=Urls.HOST_MONGO.getPath();
		String host1=Urls.HOST_MONGO1.getPath(); 
		String host2=Urls.HOST_MONGO2.getPath(); 
		
		int puerto=Integer.parseInt(Urls.PUERTO_MONGO.getPath());
		
		System.out.println("base de datos---------"+baseDatos);
		System.out.println("usu-------------------"+usu);
		System.out.println("pwd-------------------"+pwd);
		System.out.println("host------------------"+host);
		System.out.println("host1-----------------"+host1);
		System.out.println("host2-----------------"+host2);
		System.out.println("puerto----------------"+puerto);
		
		
      MongoCredential mongoCredential = MongoCredential.createScramSha1Credential("admin","test",
		    "admin".toCharArray());

      mongo = new MongoClient(Arrays.asList(new ServerAddress("mongodb-0.mongodb-internal.minio.svc.cluster.local", 27017),
    		  					new ServerAddress("mongodb-1.mongodb-internal.minio.svc.cluster.local", 27017),
    		  					new ServerAddress("mongodb-2.mongodb-internal.minio.svc.cluster.local", 27017)), Arrays.asList(mongoCredential)); 
      
		return mongo;
	}

}
